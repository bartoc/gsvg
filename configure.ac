AC_PREREQ([2.69])


dnl ***********************************************************************
dnl Define Versioning Information
dnl
dnl major_version is API/ABI version. Change means API/ABI break
dnl minor_version odd is unstable API/ABI, even is stable API/ABI
dnl micro_version means fixes with no API/ABI break
dnl ***********************************************************************
m4_define([major_version],[0])
m4_define([minor_version],[5])
m4_define([micro_version],[1])
m4_define([project_version],[major_version.minor_version.micro_version])
m4_define([bug_report_url],[https://gitlab.com/pwmc/gsvg/issues])
m4_define([api_version],[0.6])

# LT_VERSION for ABI related changes
# From: https://autotools.io/libtool/version.html
# This rules applies to Meson 0.43
# Increase the current value whenever an interface has been added, removed or changed.
# Always increase revision value whenever an interface has been added, removed or changed.
# Increase the age value only if the changes made to the ABI are backward compatible.
# Set version to the value of subtract age from current
# Reset current and version to 1 and, age and version to 0 if library's name is changed
m4_define([project_lt_current], [1])
m4_define([project_lt_revision], [0])
m4_define([project_lt_age], [0])

AX_IS_RELEASE([micro-version])

dnl ***********************************************************************
dnl Initialize autoconf
dnl ***********************************************************************
AC_INIT([gsvg],[package_version],[bug_report_url])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_SRCDIR([data/gsvg.pc.in])
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_AUX_DIR([build-aux])
AC_SUBST([ACLOCAL_AMFLAGS], "-I m4")
AC_CANONICAL_HOST


dnl ***********************************************************************
dnl Make version information available to autoconf files
dnl ***********************************************************************
AC_SUBST([MAJOR_VERSION],major_version)
AC_SUBST([MINOR_VERSION],minor_version)
AC_SUBST([MICRO_VERSION],micro_version)
AC_SUBST([API_VERSION],api_version)

LT_CURRENT=project_lt_current
LT_REVISION=project_lt_revision
LT_AGE=project_lt_age
AC_SUBST([LT_CURRENT])
AC_SUBST([LT_REVISION])
AC_SUBST([LT_AGE])

dnl ***********************************************************************
dnl Initialize automake
dnl ***********************************************************************
AM_SILENT_RULES([yes])
AM_INIT_AUTOMAKE([1.11 dist-xz no-define
                  no-dist-gzip tar-ustar -Wno-portability])
AM_MAINTAINER_MODE([enable])
AX_GENERATE_CHANGELOG


dnl ***********************************************************************
dnl Add extra debugging with --enable-debug and --enable-compile-warnings
dnl ***********************************************************************
AX_CHECK_ENABLE_DEBUG([no],[]
                      [G_DISABLE_ASSERT G_DISABLE_CHECKS G_DISABLE_CAST_CHECKS])


dnl ***********************************************************************
dnl Internationalization
dnl ***********************************************************************
GETTEXT_PACKAGE=AC_PACKAGE_TARNAME
AC_SUBST([GETTEXT_PACKAGE])
AC_DEFINE_UNQUOTED([GETTEXT_PACKAGE], ["$GETTEXT_PACKAGE"], [GETTEXT package name])

AM_GNU_GETTEXT_VERSION([0.19.3])
AM_GNU_GETTEXT([external])


dnl ***********************************************************************
dnl Check for required programs
dnl ***********************************************************************
AC_PROG_CC
AC_PROG_INSTALL
AC_PATH_PROG([GLIB_GENMARSHAL],[glib-genmarshal])
AC_PATH_PROG([GLIB_MKENUMS],[glib-mkenums])
AC_PATH_PROG([GLIB_COMPILE_RESOURCES],[glib-compile-resources])
PKG_PROG_PKG_CONFIG([0.22])
GLIB_GSETTINGS
GOBJECT_INTROSPECTION_CHECK([1.42.0])

VALAC_CHECK

VERSION=project_version
AC_SUBST(VERSION)
API_VERSION=api_version
AC_SUBST(API_VERSION)
PROJECT_NAME=gsvg
AC_SUBST(PROJECT_NAME)
CAMEL_CASE_NAME=GSvg
AC_SUBST(CAMEL_CASE_NAME)
DESCRIPTION="GObject bindings for W3C SVG 1.1"
AC_SUBST(DESCRIPTION)

AX_COMPILER_FLAGS


dnl ***********************************************************************
dnl Ensure C11 is Supported
dnl ***********************************************************************
AX_CHECK_COMPILE_FLAG([-std=gnu11],
                      [CFLAGS="$CFLAGS -std=gnu11"],
                      [AC_MSG_ERROR([C compiler cannot compile GNU C11 code])])


dnl ***********************************************************************
dnl Check for required packages
dnl ***********************************************************************
PKG_CHECK_MODULES(GSVG, [gxml-0.16 >= 0.15.3])
dnl ***********************************************************************
dnl Initialize Libtool
dnl ***********************************************************************
LT_PREREQ([2.2])

# Documentation with Valadoc
AC_PATH_PROG([VALADOC], [valadoc], [no])
AC_PATH_PROG([GRESG], [gresg], [no])

docs=yes
AC_ARG_ENABLE(docs,
	AS_HELP_STRING([--enable-docs], [Enable Docs option [default=no]]),
        [docs=$enableval], [docs="no"])

have_valadoc=no
if test "x$VALADOC" = "xno" -o "x$docs" = "xno"; then
	have_valadoc="no"
else
	have_valadoc="yes"
fi
AM_CONDITIONAL([HAVE_VALADOC], [test x$have_valadoc = xyes])

found_resource_compiler="no"
AC_PATH_PROG([RESOURCE_COMPILER], glib-compile-resources, [no])
if test "x$RESOURCE_COMPILER" = "xno"
then
  AC_MSG_ERROR([GLib resource compiler required])
else
  AC_SUBST(RESOURCE_COMPILER)
  found_resource_compiler="yes"
fi


dnl ***************************************************************************
dnl Check for Windows
dnl ***************************************************************************
AC_CANONICAL_HOST

platform_win32=no
platform_win64=no
cross_compiling=no
case "$host" in
*-mingw*)
    platform_win32=yes
    cross_compiling=yes
    case "$host" in
    *x86_64*)
      platform_win64=yes
      ;;
    *)
    esac
    ;;
*)
esac
AM_CONDITIONAL(PLATFORM_WIN, [test $platform_win32 = yes -o  $platform_win64 = yes])
AM_CONDITIONAL(PLATFORM_WIN32, [test $platform_win32 = yes])
AM_CONDITIONAL(PLATFORM_WIN64, [test $platform_win64 = yes])

dnl ***********************************************************************
dnl Adding win32 DLL generation
dnl ***********************************************************************¡
LT_INIT([win32-dll])


debug=no
AC_ARG_ENABLE(debug,
	AS_HELP_STRING([--enable-debug], [Enable Debug [default=no]]),
        [debug=$enableval], [debug="no"])

AM_CONDITIONAL(DEBUG, [test $debug = yes])

dnl ***********************************************************************
dnl Process .in Files
dnl ***********************************************************************
AC_CONFIG_FILES([
	Makefile

	src/Makefile
	src/namespace-info.vala
	src/gsvg-$API_VERSION.deps:src/gsvg.deps.in

	data/Makefile
	data/gsvg-$API_VERSION.pc:data/gsvg.pc.in

	po/Makefile.in
],[],
[API_VERSION='$API_VERSION'])
AC_OUTPUT

echo ""
echo " ${PACKAGE} - ${VERSION}"
echo ""
echo " Options"
echo ""
echo "  Prefix ............................... : ${prefix}"
echo "  Libdir ............................... : ${libdir}"
echo "  debug ................................ : ${debug}"
echo ""

