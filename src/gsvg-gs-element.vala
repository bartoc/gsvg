/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;
using Gee;


public class GSvg.GsElement : GSvg.GsObject,
                        GSvg.Element,
                        GSvg.Stylable
{
  protected SVGElement _owner_svg_element;
  protected Element _viewport_element;
  protected AnimatedString _class;
  protected CSSStyleDeclaration _style;
  // Element
  [Description (nick="::xml:base")]
  public string xmlbase { get; set; }
  public SVGElement? owner_svg_element {
    get { return _owner_svg_element; }
  }
  public Element? viewport_element { get { return _viewport_element; } }
  // Stylable
  [Description (nick="::class")]
  public AnimatedString? class_name {
    get { return _class; } construct set { _class = value; }
  }
  [Description (nick="::style")]
  public CSSStyleDeclaration? style {
    get {
      if (_style == null)
        _style = new GsCSSStyleDeclaration () as CSSStyleDeclaration;
      return _style;
    }
    construct set { _style = value; }
  }

  public CSSValue? get_presentation_attribute (string name) { return null; }

  // Styling properties
  // Fonts properties
  [Description (nick="::font")]
  public string font { get; set; }
  [Description (nick="::font-family")]
  public string font_family { get; set; }
  [Description (nick="::font-size")]
  public string font_size { get; set; }
  [Description (nick="::font-size-adjust")]
  public string font_size_adjust { get; set; }
  [Description (nick="::font-stretch")]
  public string font_stretch { get; set; }
  [Description (nick="::font-style")]
  public string font_style { get; set; }
  [Description (nick="::font-variant")]
  public string font_variant { get; set; }
  [Description (nick="::font-weight")]
  public string font_weight { get; set; }
  // Text properties
  [Description (nick="::direction")]
  public string direction { get; set; }
  [Description (nick="::letter-spacing")]
  public string letter_spacing { get; set; }
  [Description (nick="::text-decoration")]
  public string text_decoration { get; set; }
  [Description (nick="::unicode-bidi")]
  public string unicode_bidi { get; set; }
  [Description (nick="::word-spacing")]
  public string word_spacing { get; set; }
  // Other visual properties
  [Description (nick="::clip")]
  public string clip { get; set; }
  [Description (nick="::color")]
  public string color { get; set; }
  [Description (nick="::cursor")]
  public string cursor { get; set; }
  [Description (nick="::display")]
  public string display { get; set; }
  [Description (nick="::overflow")]
  public string overflow { get; set; }
  [Description (nick="::visibility")]
  public string visibility { get; set; }
  // Clipping, Masking and Compositing properties
  [Description (nick="::clip-path")]
  public string clip_path { get; set; }
  [Description (nick="::clip-rule")]
  public string clip_rule { get; set; }
  [Description (nick="::mask")]
  public string mask { get; set; }
  [Description (nick="::opacity")]
  public string opacity { get; set; }
  // Filter Effects properties
  [Description (nick="::enable-background")]
  public string enable_background { get; set; }
  [Description (nick="::filter")]
  public string filter { get; set; }
  [Description (nick="::flood-color")]
  public string flood_color { get; set; }
  [Description (nick="::flood-opacity")]
  public string flood_opacity { get; set; }
  [Description (nick="::lighting-color")]
  public string lighting_color { get; set; }
  // Gradient properties
  [Description (nick="::stop-color")]
  public string stop_color { get; set; }
  [Description (nick="::stop-opacity")]
  public string stop_opacity { get; set; }
  // Interactivity properties
  [Description (nick="::pointer-events")]
  public string pointer_events { get; set; }
  // Color and painting properties
  [Description (nick="::color-interpolation")]
  public string color_interpolation { get; set; }
  [Description (nick="::color-interpolation-filter")]
  public string color_interpolation_filter { get; set; }
  [Description (nick="::color-profile")]
  public string color_profile { get; set; }
  [Description (nick="::color-rendering")]
  public string color_rendering { get; set; }
  [Description (nick="::fill")]
  public string fill { get; set; }
  [Description (nick="::fill-opacity")]
  public string fill_opacity { get; set; }
  [Description (nick="::fill-rule")]
  public string fill_rule { get; set; }
  [Description (nick="::image-rendering")]
  public string image_rendering { get; set; }
  [Description (nick="::marker")]
  public string marker { get; set; }
  [Description (nick="::maker-end")]
  public string maker_end { get; set; }
  [Description (nick="::marker_mid")]
  public string maker_mid { get; set; }
  [Description (nick="::maker-start")]
  public string maker_start { get; set; }
  [Description (nick="::shape-rendering")]
  public string shape_rendering { get; set; }
  [Description (nick="::stroke")]
  public string stroke { get; set; }
  [Description (nick="::stroke-dasharray")]
  public string stroke_dasharray { get; set; }
  [Description (nick="::stroke-dashoffset")]
  public string stroke_dashoffset { get; set; }
  [Description (nick="::stroke-linecap")]
  public string stroke_linecap { get; set; }
  [Description (nick="::stroke-linejoin")]
  public string stroke_linejoin { get; set; }
  [Description (nick="::stroke-miterlimit")]
  public string stroke_miterlimit { get; set; }
  [Description (nick="::stroke-opacity")]
  public string stroke_opacity { get; set; }
  [Description (nick="::stroke-width")]
  public string stroke_width { get; set; }
  [Description (nick="::text-rendering")]
  public string text_rendering { get; set; }
  // Other text properties
  [Description (nick="::alignment-baseline")]
  public string alignment_baseline { get; set; }
  [Description (nick="::baseline-shift")]
  public string baseline_shift { get; set; }
  [Description (nick="::dominant-baseline")]
  public string dominant_baseline { get; set; }
  [Description (nick="::glyph-orientation-horizontal")]
  public string glyph_orientation_horizontal { get; set; }
  [Description (nick="::glyph-orientation-vertical")]
  public string glyph_orientation_vertical { get; set; }
  [Description (nick="::kerning")]
  public string kerning { get; set; }
  [Description (nick="::text-anchor")]
  public string text_anchor { get; set; }
  [Description (nick="::writing-mode")]
  public string writing_mode { get; set; }
}