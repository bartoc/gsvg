/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;
using Gee;

/**
 * A node map for {@link GElement} objects with id as its key.
 */
public class GSvg.GsGElementMap : GomHashMap, GElementMap {
  public int length { get { return (this as GomHashMap).length; } }
  construct {
    try {
      initialize (typeof (GsGElement));
    } catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
  public new GElement GElementMap.get (string id) {
    return (this as GomHashMap).get (id) as GElement;
  }
}