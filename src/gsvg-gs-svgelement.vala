/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;
using Gee;

/**
 * 'svg' node.
 */
public class GSvg.GsSVGElement : GSvg.GsContainerElement,
                        FitToViewBox,
                        ZoomAndPan,
                        ViewCSS,
                        DocumentCSS,
                        GSvg.SVGElement, MappeableElement
{
  protected float _pixel_unit_to_millimeter_x;
  protected float _pixel_unit_to_millimeter_y;
  protected float _screen_pixel_to_millimeter_x;
  protected float _screen_pixel_to_millimeter_y;
  protected bool _use_current_view;
  protected ViewSpec _current_view;
  protected Point _current_translate;

  // FitToViewBox
  // * viewBox
  public AnimatedRect view_box { get; set; }
  [Description (nick="::viewBox")]
  public GsAnimatedRect mview_box {
    get { return view_box as GsAnimatedRect; }
    set { view_box = value as AnimatedRect; }
  }
  // preserveAspectRatio
  public AnimatedPreserveAspectRatio preserve_aspect_ratio { get; set; }
  [Description (nick="::preserveAspectRatio")]
  public GsAnimatedPreserveAspectRatio mpreserve_aspect_ratio {
    get { return preserve_aspect_ratio as GsAnimatedPreserveAspectRatio; }
    set { preserve_aspect_ratio = value as AnimatedPreserveAspectRatio; }
  }

  // ZoomAndPan
  // * zoomAndPan
  public int zoom_and_pan { get; set; }
  // ViewCSS
  public CSSStyleDeclaration get_computed_style (DomElement elt,
                                                      string pseudoElt)
  {
    return style; // FIXME
  }
  // DocumentCSS
  public CSSStyleDeclaration get_override_style (DomElement elt,
                                                string pseudoElt)
  {
    return style; // FIXME
  }
  // SVGElement
  public AnimatedLength x { get; set; }
  [Description (nick="::x")]
  public GsAnimatedLength mx {
    get { return x as GsAnimatedLength; }
    set { x = value as AnimatedLength; }
  }
  public AnimatedLength y { get; set; }
  [Description (nick="::y")]
  public GsAnimatedLength my {
    get { return y as GsAnimatedLength; }
    set { y = value as AnimatedLength; }
  }
  public AnimatedLength width { get; set; }
  [Description (nick="::width")]
  public GsAnimatedLength mwidth {
    get { return width as GsAnimatedLength; }
    set { width = value as AnimatedLength; }
  }
  public AnimatedLength height { get; set; }
  [Description (nick="::height")]
  public GsAnimatedLength mheight {
    get { return height as GsAnimatedLength; }
    set { height = value as AnimatedLength; }
  }
  [Description (nick="::contentScriptType")]
  public string content_script_type { get; set; }
  [Description (nick="::contentStyleType")]
  public string content_style_type { get; set; }
  public Rect viewport { get; set; }
  [Description (nick="::viewport")]
  public GsRect mviewport {
    get { return viewport as GsRect; }
    set { viewport = value as Rect; }
  }
  //pixelUnitToMillimeterX
  public float pixel_unit_to_millimeter_x { get { return _pixel_unit_to_millimeter_x; } }
  //pixelUnitToMillimeterY
  public float pixel_unit_to_millimeter_y { get { return _pixel_unit_to_millimeter_y; } }
  //screenPixelToMillimeterX
  public float screen_pixel_to_millimeter_x { get { return _screen_pixel_to_millimeter_x; } }
  //screenPixelToMillimeterY
  public float screen_pixel_to_millimeter_y { get { return _screen_pixel_to_millimeter_y; } }
  //useCurrentView
  public bool use_current_view { get { return _use_current_view; } }
  //currentView
  public ViewSpec current_view { get { return _current_view; } }
  //currentScale
  public float current_scale { get; set; }
  // currentTranslate
  public Point current_translate { get { return _current_translate; } }

  construct {
    _local_name = "svg";
    try {
      set_attribute_ns ("http://www.w3.org/2000/xmlns/",
                      "xmlns", "http://www.w3.org/2000/svg");
    } catch (GLib.Error e) {
      warning (("Error setting default namespace: %s").printf (e.message));
    }
  }

  //currentTranslate
  public  uint suspend_redraw (uint maxWaitMilliseconds) { return 0; }
  public  void unsuspend_redraw (uint suspendHandleID) {}
  public  void unsuspend_redrawAll () {}
  public  void force_redraw () {}
  public  void pause_animations () {}
  public  void unpause_animations () {}
  public  bool animations_paused () { return false; }
  public  float get_current_time () { return (float) 0.0; }
  public  void set_current_time (float seconds) {}
  public  DomNodeList get_intersection_list (Rect rect, Element referenceElement) {
    return new GomNodeList () as DomNodeList;
  }
  public  DomNodeList get_enclosure_list (Rect rect, Element referenceElement) {
    return new GomNodeList () as DomNodeList;
  }
  public  bool check_intersection (Element element, Rect rect) { return false; }
  public  bool check_enclosure (Element element, Rect rect) { return false; }
  public  void deselect_all () {}
  // Creators
  public  Number create_svg_number () { return new GsNumber (); }
  public  Length create_svg_length () { return new GsLength (); }
  public  Angle create_svg_angle () { return new GsAngle (); }
  public  Point create_svg_point () { return new GsPoint (); }
  public  Matrix create_svg_matrix() { return new GsMatrix (); }
  public  Rect create_svg_rect () {
    var r = new GsRectElement ();
    r.x = new GsAnimatedLengthX ();
    r.y = new GsAnimatedLengthY ();
    r.width = new GsAnimatedLengthWidth ();
    r.height = new GsAnimatedLengthHeight ();
    r.rx = new GsAnimatedLengthRX ();
    r.ry = new GsAnimatedLengthRY ();
    return r as Rect;
  }
  public  Transform create_svg_transform () { return new GsTransform (); } // FIXME
  public  Transform create_svg_transform_from_matrix (Matrix matrix) { return new GsTransform (); } //FIXME
  /**
   * Query elements by 'id' property
   */
  public  DomElement? get_element_by_id (string element_id) {
    try {
      var l2 = owner_document.get_element_by_id (element_id);
      if (l2 != null) return l2;
      var l = get_elements_by_property_value ("id", element_id);
      if (l.length == 0) return null;
      message (l.get_element (0).node_name);

      return  l.get_element (0);
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return null;
  }
  // Shape constructors
  public RectElement create_rect (string? x,
                                  string? y,
                                  string? width,
                                  string? height,
                                  string? rx,
                                  string? ry,
                                  string? style = null) {
    GsAnimatedLengthX nx = null;
    if (x != null) {
      nx = new GsAnimatedLengthX ();
      nx.value = x;
    }
    GsAnimatedLengthY ny = null;
    if (y != null) {
      ny = new GsAnimatedLengthY ();
      ny.value = y;
    }
    GsAnimatedLengthWidth nw = null;
    if (width != null) {
      nw = new GsAnimatedLengthWidth();
      nw.value = width;
    }
    GsAnimatedLengthHeight nh = null;
    if (height != null) {
      nh = new GsAnimatedLengthHeight ();
      nh.value = height;
    }
    GsAnimatedLengthRX nrx = null;
    if (rx != null) {
      nrx = new GsAnimatedLengthRX ();
      nrx.value = rx;
    }
    GsAnimatedLengthRY nry = null;
    if (ry != null) {
      nry = new GsAnimatedLengthRY ();
      nry.value = ry;
    }
    var r = Object.new (typeof (GsRectElement),
                        "owner_document", this.owner_document)
                        as RectElement;
    r.x = nx;
    r.y = ny;
    r.width = nw;
    r.height = nh;
    r.rx = nrx;
    r.ry = nry;
    return r;
  }
  public CircleElement create_circle (string? cx,
                                  string? cy,
                                  string? cr,
                                  string? style = null) {
    GsAnimatedLengthCX nx = null;
    if (cx != null) {
      nx = new GsAnimatedLengthCX ();
      nx.value = cx;
    }
    GsAnimatedLengthCY ny = null;
    if (cy != null) {
      ny = new GsAnimatedLengthCY ();
      ny.value = cy;
    }
    GsAnimatedLengthR nr = null;
    if (cr != null) {
      nr = new GsAnimatedLengthR ();
      nr.value = cr;
    }
    var c = Object.new (typeof (GsCircleElement),
                        "owner_document", this.owner_document)
                        as CircleElement;
    c.cx = nx;
    c.cy = ny;
    c.r = nr;
    return c;
  }
  public EllipseElement create_ellipse (string? cx,
                                  string? cy,
                                  string? crx,
                                  string? cry,
                                  string? style = null) {
    GsAnimatedLengthCX nx = null;
    if (cx != null) {
      nx = new GsAnimatedLengthCX ();
      nx.value = cx;
    }
    GsAnimatedLengthCY ny = null;
    if (cy != null) {
      ny = new GsAnimatedLengthCY ();
      ny.value = cy;
    }
    GsAnimatedLengthRX nrx = null;
    if (crx != null) {
      nrx = new GsAnimatedLengthRX ();
      nrx.value = crx;
    }
    GsAnimatedLengthRY nry = null;
    if (cry != null) {
      nry = new GsAnimatedLengthRY ();
      nry.value = cry;
    }
    var e = Object.new (typeof (GsEllipseElement),
                        "owner_document", this.owner_document)
                        as EllipseElement;
    e.cx = nx;
    e.cy = ny;
    e.rx = nrx;
    e.ry = nry;
    return e;
  }
  public LineElement create_line (string? lx1,
                                  string? ly1,
                                  string? lx2,
                                  string? ly2,
                                  string? style = null) {
    GsAnimatedLengthX nx1 = null;
    if (lx1 != null) {
      nx1 = new GsAnimatedLengthX ();
      nx1.value = lx1;
    }
    GsAnimatedLengthY ny1 = null;
    if (ly1 != null) {
      ny1 = new GsAnimatedLengthY ();
      ny1.value = ly1;
    }
    GsAnimatedLengthX nx2 = null;
    if (lx2 != null) {
      nx2 = new GsAnimatedLengthX ();
      nx2.value = lx2;
    }
    GsAnimatedLengthY ny2 = null;
    if (ly2 != null) {
      ny2 = new GsAnimatedLengthY ();
      ny2.value = ly2;
    }
    var l = Object.new (typeof (GsLineElement),
                        "owner_document", this.owner_document)
                        as GsLineElement;
    l.x1 = nx1;
    l.y1 = ny1;
    l.x2 = nx2;
    l.y2 = ny2;
    return l;
  }
  public PolylineElement create_polyline (string points,
                                   string? style = null) {
    var l = Object.new (typeof (GsPolylineElement),
                        "owner_document", this.owner_document)
                        as PolylineElement;
    l.points.value = points;
    return l;
  }
  public PolygonElement create_polygon (string points,
                                   string? style = null) {
    var l = Object.new (typeof (GsPolygonElement),
                        "owner_document", this.owner_document)
                        as PolygonElement;
    l.points.value = points;
    return l;
  }
  public TextElement create_text (string? text,
                                   string? xs,
                                   string? ys,
                                   string? dxs,
                                   string? dys,
                                   string? rotates,
                                   string? style = null) {
    var t = Object.new (typeof (GsTextElement),
                        "owner_document", this.owner_document)
                        as GsTextElement;
    if (text != null)
      t.add_text (text);
    if (xs != null) {
      t.x = new GsAnimatedLengthList ();
      t.x.value = xs;
    }
    if (ys != null) {
      t.y = new GsAnimatedLengthList ();
      t.y.value = ys;
    }
    if (dxs != null) {
      t.dx = new GsAnimatedLengthList ();
      t.dx.value = dxs;
    }
    if (dys != null) {
      t.dy = new GsAnimatedLengthList ();
      t.dy.value = dys;
    }
    if (rotates != null) {
      t.rotate = new GsAnimatedNumberList ();
      t.rotate.value = rotates;
    }
    return t;
  }
  public GElement create_g () {
    var g = Object.new (typeof (GsGElement),
                        "owner_document", owner_document)
                        as GsGElement;
    return g;
  }
  public DefsElement add_defs () {
    var d = Object.new (typeof (GsDefsElement),
                        "owner_document", owner_document)
                        as GsDefsElement;
    try { append_child (d); }
    catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return d;
  }
  public GElement add_g () {
    var g = create_g ();
    try { append_child (g); }
    catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return g;
  }
  public TitleElement add_title (string text) {
    var t = Object.new (typeof (GsTitleElement),
                        "owner_document", owner_document)
                        as GsTitleElement;
    try {
      var tx = owner_document.create_text_node (text);
      t.append_child (tx);
      append_child (t);
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return t;
  }
  public DescElement add_desc (string? text) {
    var t = Object.new (typeof (GsDescElement),
                        "owner_document", owner_document)
                        as GsDescElement;
    try {
      if (text != null) {
        var tx = owner_document.create_text_node (text);
        t.append_child (tx);
      }
      append_child (t);
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return t;
  }
  public MetadataElement add_metadata () {
    var mt = Object.new (typeof (GsMetadataElement),
                        "owner_document", owner_document)
                        as GsMetadataElement;
    try { append_child (mt); }
    catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return mt;
  }
  // MappeableElement
  public string get_map_key () { return id; }
}