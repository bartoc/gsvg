/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;
using Gee;

public class GSvg.GsRect : Object, GomProperty, Rect {
  public double x { get; set; }
  public double y { get; set; }
  public double width { get; set; }
  public double height { get; set; }

  public string? value {
    owned get { return to_string (); }
    set { parse (value); }
  }

  public bool validate_value (string str) {
    return double.try_parse (str);
  }

  public void parse (string str) {
    if (!(" " in str)) {
      x = double.parse (str);
      return;
    }
    string[] st = str.split (" ");
    for (int i = 0; i < st.length; i++) {
      if (i == 0) x = double.parse (st[0]);
      if (i == 1) y = double.parse (st[1]);
      if (i == 2) width = double.parse (st[2]);
      if (i == 3) height = double.parse (st[3]);
    }
  }
  public string to_string () {
    return "%0.0g".printf (x)+" "+"%0.0g".printf (y)
        +" "+"%0.0g".printf (width)+" "+"%0.0g".printf (height);
  }
}

public class GSvg.GsAnimatedRect : Object,
                                GomProperty,
                                AnimatedRect
{
  private Rect _base_val;
  private Rect _anim_val;
  public Rect base_val {
    get {
      if (_base_val == null) _base_val = new GsRect ();
      return _base_val;
    }
  }
  public Rect anim_val {
    get {
      if (_anim_val == null) _anim_val = new GsRect ();
      return _anim_val;
    }
  }
  public string? value {
    owned get { return base_val.to_string (); }
    set { base_val.parse (value); }
  }
  public bool validate_value (string str) {
    return _base_val.validate_value (str); // FIXME:
  }
}


public class GSvg.GsPreserveAspectRatio : Object, PreserveAspectRatio {
  private string defer = null;
  public PreserveAspectRatio.Type align { get; set; }
  public PreserveAspectRatio.MeetorSlice meet_or_slice { get; set; }
  public string? to_string () {
    if (PreserveAspectRatio.Type.to_string (align) == null) return null;
    string str = "";
    if (defer != null)
      str += defer+" ";
    str += PreserveAspectRatio.Type.to_string (align);
    if (meet_or_slice != PreserveAspectRatio.MeetorSlice.UNKNOWN)
      str += " "+PreserveAspectRatio.MeetorSlice.to_string (meet_or_slice);
    return str;
  }
  public void parse (string str) {
    if (str == "") return;
    string a = str;
    string m = "";
    if (" " in str) {
      string[] st = str.split (" ");
      if (st.length == 0) return;
      int p = 0;
      if ("defer" == st[0].down ()) {
        defer = st[0];
        p++;
      }
      a = st[p];
      if (p+1 < st.length)
        p++;
      m = st[p];
    }
    align = PreserveAspectRatio.Type.parse (a);
    meet_or_slice = PreserveAspectRatio.MeetorSlice.parse (m);
  }
}

public class GSvg.GsAnimatedPreserveAspectRatio : Object, GomProperty, AnimatedPreserveAspectRatio {
  public PreserveAspectRatio base_val { get; set; }
  public PreserveAspectRatio anim_val { get; set; }
  public string? value {
    owned get {
      if (base_val == null)
        return null;
      return (base_val as GsPreserveAspectRatio).to_string ();
    }
    set {
      if (base_val == null)
        base_val = new GsPreserveAspectRatio () as PreserveAspectRatio;
      (base_val as GsPreserveAspectRatio).parse (value);
    }
  }
  public bool validate_value (string val) {
    return true; // FIXME
  }
}

public class GSvg.GsDescriptiveElement : GSvg.GsElement,
                                        LangSpace
{
  // LangSpace
  [Description (nick="::xml:lang")]
  public string xmllang { get; set; }
  [Description (nick="::xml:space")]
  public string xmlspace { get; set; }
}

public class GSvg.GsTitleElement : GSvg.GsDescriptiveElement, TitleElement {
  construct { initialize ("title"); }
}

public class GSvg.GsDescElement : GSvg.GsDescriptiveElement, DescElement {
  construct { initialize ("desc"); }
}

public class GSvg.GsMetadataElement : GsElement, MetadataElement {
  construct { initialize ("metadata"); }
}

