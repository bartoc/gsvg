/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;
using Gee;

/**
 * Creates an stand alone SVG document, without a top level 'svg' element.
 *
 * You should use {@link add_svg} to add a new 'svg' node.
 */
public class GSvg.GsDocument : GXml.GomDocument,  GSvg.Document {
  protected string _referrer = "";
  protected string _domain = "";
  public string title {
    owned get {
      if (document_element == null) return "";
      if (document_element.local_name.down () != "svg") return "";
      var els = document_element.get_elements_by_tag_name ("title");
      if (els.length == 0) return "";
      return els.item (0).text_content;
    }
  }
  public string referrer {
    get { return _referrer; }
  }
  public string domain { get { return _domain; } }
  public string url { get { return _url; } }
  public SVGElement? root_element {
    owned get {
      var r = ((DomDocument) this).document_element;
      if (r is SVGElement) return (SVGElement) r;
      foreach (DomNode n in children) {
        if (n is SVGElement) return (SVGElement) n;
      }
      return null;
    }
  }
  construct {
    var dt = new GomDocumentType (this, "svg", "-//W3C//DTD SVG 1.1//EN",
  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd");
    try { append_child (dt); } catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
  public new void read_from_string (string str) throws GLib.Error {
    add_svg (null, null, null, null);
    root_element.read_from_string (str);
  }
  public new void read_from_file (GLib.File file) throws GLib.Error {
    add_svg (null, null, null, null);
    root_element.read_from_file (file);
  }
  public SVGElement add_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null)
  {
    var s = create_svg (x, y, width, height, viewbox, title, desc);
    try {
      if (root_element == null)
        append_child (s);
      else
        root_element.append_child (s);
    } catch (GLib.Error e) { warning ("Error: "+e.message); }

    return s;
  }
  public SVGElement create_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null)
  {
    var nsvg = Object.new (typeof (GSvg.GsSVGElement),
                          "owner-document", this) as SVGElement;
    if (x != null) {
      nsvg.x = new GsAnimatedLength ();
      nsvg.x.value = x;
    }
    if (y != null) {
      nsvg.y = new GsAnimatedLength ();
      nsvg.y.value = y;
    }
    if (width != null) {
      nsvg.width = new GsAnimatedLength ();
      nsvg.width.value = width;
    }
    if (height != null) {
      nsvg.height = new GsAnimatedLength ();
      nsvg.height.value = height;
    }
    if (viewbox != null) {
      nsvg.view_box = new GsAnimatedRect ();
      nsvg.view_box.value = viewbox;
    }
    if (title != null) {
      nsvg.add_title (title);
    }
    if (desc != null) {
      nsvg.add_desc (desc);
    }
    return nsvg;
  }
}
