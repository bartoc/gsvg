/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;
using Gee;

/**
 * An implementation of {@link ContainerElement}
 */
public class GSvg.GsContainerElement : GSvg.GsCommonShapeElement, ContainerElement {
  // ContainerElement
  private GsSVGElementMap _svgs_map;
  public SVGElementMap svgs { get { return svgs_map as SVGElementMap; } }
  public GsSVGElementMap svgs_map {
    get {
      if (_svgs_map == null)
        set_instance_property ("svgs-map");
      return _svgs_map;
    }
    set {
      if (_svgs_map != null) {
        try {
          clean_property_elements ("svgs-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _svgs_map = value;
    }
  }
  private GsGElementMap _groups_map;
  public GElementMap groups { get { return groups_map as GElementMap; } }
  public GsGElementMap groups_map {
    get {
      if (_groups_map == null)
        set_instance_property ("groups-map");
      return _groups_map;
    }
    set {
      if (_groups_map != null) {
        try {
          clean_property_elements ("groups-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _groups_map = value;
    }
  }
  private GsRectElementMap _rects_map;
  public RectElementMap rects { get { return rects_map as RectElementMap; } }
  public GsRectElementMap rects_map {
    get {
      if (_rects_map == null)
        set_instance_property ("rects-map");
      return _rects_map;
    }
    set {
      if (_rects_map != null) {
        try {
          clean_property_elements ("rects-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _rects_map = value;
    }
  }
  private GsCircleElementMap _circles_map;
  public CircleElementMap circles { get { return circles_map as CircleElementMap; } }
  public GsCircleElementMap circles_map {
    get {
      if (_circles_map == null)
        set_instance_property ("circles-map");
      return _circles_map;
    }
    set {
      if (_circles_map != null) {
        try {
          clean_property_elements ("circles-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _circles_map = value;
    }
  }
  private GsEllipseElementMap _ellipses_map;
  public EllipseElementMap ellipses { get { return ellipses_map as EllipseElementMap; } }
  public GsEllipseElementMap ellipses_map {
    get {
      if (_ellipses_map == null)
        set_instance_property ("ellipses-map");
      return _ellipses_map;
    }
    set {
      if (_ellipses_map != null) {
        try {
          clean_property_elements ("ellipses-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _ellipses_map = value;
    }
  }
  private GsLineElementMap _lines_map;
  public LineElementMap lines { get { return lines_map as LineElementMap; } }
  public GsLineElementMap lines_map {
    get {
      if (_lines_map == null)
        set_instance_property ("lines-map");
      return _lines_map;
    }
    set {
      if (_lines_map != null) {
        try {
          clean_property_elements ("lines-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _lines_map = value;
    }
  }
  private GsPolylineElementMap _polylines_map;
  public PolylineElementMap polylines { get { return polylines_map as PolylineElementMap; } }
  public GsPolylineElementMap polylines_map {
    get {
      if (_polylines_map == null)
        set_instance_property ("polylines-map");
      return _polylines_map;
    }
    set {
      if (_polylines_map != null) {
        try {
          clean_property_elements ("polylines-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _polylines_map = value;
    }
  }
  private GsPolygonElementMap _polygons_map;
  public PolygonElementMap polygons { get { return polygons_map as PolygonElementMap; } }
  public GsPolygonElementMap polygons_map {
    get {
      if (_polygons_map == null)
        set_instance_property ("polygons-map");
      return _polygons_map;
    }
    set {
      if (_polygons_map != null) {
        try {
          clean_property_elements ("polygons-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _polygons_map = value;
    }
  }
  private GsTextElementMap _texts_map;
  // ContainerElement
  public TextElementMap texts { get { return texts_map as TextElementMap; } }
  public GsTextElementMap texts_map {
    get {
      if (_texts_map == null)
        set_instance_property ("texts-map");
      return _texts_map;
    }
    set {
      if (_texts_map != null) {
        try {
          clean_property_elements ("texts-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _texts_map = value;
    }
  }
}