/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-containers-test.vala

 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;

class GSvgTest.Suite : Object
{
  static int main (string args[])
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gsvg/containers/construct",
    ()=>{
      try {
        var svg = new GSvg.GsDocument ();
        string str = """<svg/>""";
        svg.read_from_string (str);
        assert (svg.root_element != null);
        assert (svg.root_element.rects != null);
        assert (svg.root_element.lines != null);
        assert (svg.root_element.circles != null);
        assert (svg.root_element.groups != null);
        assert (svg.root_element.ellipses != null);
        assert (svg.root_element.polylines != null);
        assert (svg.root_element.polygons != null);
        assert (svg.root_element.texts != null);
        assert (svg.root_element.svgs != null);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/containers",
    ()=>{
      try {
        var svg = new GSvg.GsDocument ();
        string str = """<svg>
        <line id="line1" x1="1mm" y1="1mm" x2="1mm" y2="1mm"/>
        <rect id="rect1" x="1mm" y="1mm" width="1mm" height="1mm"/>
        <circle id="circle1" x="1mm" y="1mm" r="2mm"/>
        <g id="group1"/>
        <ellipse id="ellipse1" cx="2mm" cy="2mm" rx="5mm" ry="6mm"/>
        <polyline id="polyline1" points="(3mm,2mm) (4mm,3mm)"/>
        <polygon id="polygon1" points="(3mm,2mm) (4mm,3mm)"/>
        <text id="text1">Hello</text>
        <svg id="svg1"><line id="line1" stroke="red" stroke-width="1mm" x1="10mm" y1="10mm" x2="10mm" y2="10mm"/></svg>
        </svg>""";
        svg.read_from_string (str);
        assert (svg.root_element != null);
        assert (svg.root_element.rects != null);
        assert (svg.root_element.rects.get ("rect1") != null);
        assert (svg.root_element.lines != null);
        assert (svg.root_element.lines.get ("line1") != null);
        assert (svg.root_element.circles != null);
        assert (svg.root_element.circles.get ("circle1") != null);
        assert (svg.root_element.groups != null);
        assert (svg.root_element.groups.get ("group1") != null);
        assert (svg.root_element.ellipses != null);
        assert (svg.root_element.ellipses.get ("ellipse1") != null);
        assert (svg.root_element.polylines != null);
        assert (svg.root_element.polylines.get ("polyline1") != null);
        assert (svg.root_element.polygons != null);
        assert (svg.root_element.polygons.get ("polygon1") != null);
        assert (svg.root_element.texts != null);
        assert (svg.root_element.texts.get ("text1") != null);
        assert (svg.root_element.svgs != null);
        assert (svg.root_element.svgs.get ("svg1") != null);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    return Test.run ();
  }
}
